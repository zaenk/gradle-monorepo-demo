import {useQuery} from "@apollo/client";
import {FragmentType, gql, useFragment} from "../../src/__generated__";
import {TodoListQuery} from "../../src/__generated__/graphql";

const TodoFragment = gql(`
  fragment Todo on todo {
    id
    title
    completed
  }
`);

const TODO_LIST_QUERY = gql(`
  query todoList {
    todo(limit: 10, offset: 0) {
      ...Todo
    }
  }
`);

type TodoListProps = {
  query: TodoListQuery
}

function TodoList(props: TodoListProps) {
  const items = props.query.todo.map(todoItem => {
    const id = useFragment(TodoFragment, todoItem).id
    return <TodoItem key={id} todoItem={todoItem}/>
  })
  return <ul>{items}</ul>
}

type TodoItemProps = {
  todoItem: FragmentType<typeof TodoFragment>
}

function TodoItem(props: TodoItemProps) {
  const todoItem = useFragment(TodoFragment, props.todoItem);
  return(
    <li key={todoItem.id}>{todoItem.title}</li>
  )
}

export default function Index() {
  const {data, loading, error} = useQuery<TodoListQuery>(TODO_LIST_QUERY);
  let content;
  if (loading) content = "Loading...";
  else if (error) content = "Error!";
  else if (data === undefined) content = "Empty";
  else content = <TodoList query={data}/>;
  return (
    <div style={{fontFamily: "system-ui, sans-serif", lineHeight: "1.4"}}>
      <h1>Todo app</h1>
      {content}
    </div>
  );
}
