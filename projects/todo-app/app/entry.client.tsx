import { RemixBrowser } from "@remix-run/react";
import { startTransition, StrictMode } from "react";
import { hydrateRoot } from "react-dom/client";
import {ApolloClient, ApolloProvider, InMemoryCache} from "@apollo/client";

function Client() {
  const client = new ApolloClient({
    cache: new InMemoryCache().restore(window.__APOLLO_STATE__),
    uri: 'http://localhost:8080/v1/graphql',
  });
  return (
    <ApolloProvider client={client}>
      <StrictMode>
        <RemixBrowser />
      </StrictMode>
    </ApolloProvider>
  );
}

function hydrate() {
  startTransition(() => {
    hydrateRoot(
      document,
      <Client />
    );
  });
}

if (window.requestIdleCallback) {
  window.requestIdleCallback(hydrate);
} else {
  // Safari doesn't support requestIdleCallback
  // https://caniuse.com/requestidlecallback
  window.setTimeout(hydrate, 1);
}
