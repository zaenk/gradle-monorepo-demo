import { CodegenConfig } from '@graphql-codegen/cli';

const config: CodegenConfig = {
    schema: 'http://localhost:8080/v1/graphql',
    documents: ['app/**/*.tsx'],
    generates: {
        './src/__generated__/': {
            preset: 'client',
            plugins: [
                // 'typescript',
                // 'typescript-operations',
                // 'typescript-react-apollo',
            ],
            presetConfig: {
                gqlTagName: 'gql',
            }
        }
    },
    ignoreNoDocuments: true,
};

export default config;
