# infra / db

Runs a postgres stateful set on kubernetes

## Development

- Create a `.env` file based on the [.env.example]()
- Create users and apps in the [db.yaml]() under the `postgres-initdb` ConfigMap
- Deploy with kustomization
  ```shell
  kubectl apply -k .
  ```

## Cleanup

```shell
kubectl delete -k .
```

## Debug

- To connect from you machine, expose the service port
  ```shell
  kubectl port-forward -n db service/postgres-service 5432:5432 
  ```
