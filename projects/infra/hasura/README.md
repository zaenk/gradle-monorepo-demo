# infra / hasura

Runs a hasura gateway proxy

- It uses the official [k8s starters](https://github.com/hasura/graphql-engine/tree/stabe/install-manifests/kubernetes)
- Uses [kustomize patch](https://kubectl.docs.kubernetes.io/references/kustomize/kustomization/patches/) to set up hasura credentials

## Development

  ```shell
  kubectl apply -k .
  ```

## Cleanup

```shell
kubectl delete -k .
```

## Debug

- To connect from you machine, expose the service
  ```shell
  kubectl port-forward -n hasura service/hasura 8080:80
  ```
- To run a debug container for e.g. debugging network issues:
  ```shell
  kubectl run debug -n hasura --image=ubuntu --restart=Never -- sleep infinity
  kubectl exec -it -n hasura debug -- bash
  ```
